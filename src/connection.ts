import {Client} from 'pg';

export const client = new Client({
    user: 'postgres',
    password: process.env.DBPASSWORD, // YOU SHOULD NEVER STORE PASSWORDS IN CODE
    database: 'librarydb',
    port: 5432,
    host: '34.86.19.9'
})

client.connect();