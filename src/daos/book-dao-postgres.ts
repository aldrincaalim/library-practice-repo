import { Book } from "../entities";
import { BookDAO } from "./book-dao";
import {client} from "../connection";

export class BookDaoPostgres implements BookDAO{
    async createBook(book: Book): Promise<Book> {
        const sql:string = "insert into book(title, author, is_available, quality, return_date) values ($1,$2,$3,$4,$5) returning book_id"
        const values = [book.title, book.author, book.isAvailable, book.quality, book.returnDate];
        const result = await client.query(sql, values);
        const savedBook:Book = result.rows[0]; // it
        return savedBook;
    }

    getAllBooks(): Promise<Book[]> {
        throw new Error("Method not implemented.");
    }
    getBookById(bookId: number): Promise<Book> {
        throw new Error("Method not implemented.");
    }
    updateBook(book: Book): Promise<Book> {
        throw new Error("Method not implemented.");
    }
    deleteBookById(bookId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    
}